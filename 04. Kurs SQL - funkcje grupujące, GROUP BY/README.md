# 4. Kurs SQL - funkcje grupujące, GROUP BY

<div align="center">
    <a href="https://www.youtube.com/watch?v=P56ZjifSXW0&list=PL6aekdNhY7DA1wcv-k2MtZxasDeGlre57&index=5">
        <img src="https://i.ytimg.com/vi/P56ZjifSXW0/hqdefault.jpg" alt="1. Kurs SQL" />
    </a>
</div>

<br>

---

### Przykładowe zapytania SQL

<br>

1. ##### Wyświetla `wszystkie` kolumny w tabeli produkty

```sql
SELECT *
FROM produkty;
```

| id_produktu | producent | model   | kategoria | cena |
| :---------- | :-------- | :------ | :-------- | :--- |
| 1           | 1         | EP-200  | pralka    | 2500 |
| 2           | 1         | EP-200X | pralka    | 2750 |
| 3           | 1         | EP-300  | pralka    | 3199 |
| 4           | 1         | EE-1000 | telewizor | 3999 |
| 5           | 1         | EE-2000 | telewizor | 4499 |
| 6           | 1         | EX-550  | kuchenka  | 2100 |
| 7           | 1         | EZ-550  | lodówka   | 3500 |
| 8           | 2         | S-300   | pralka    | 2999 |
| 9           | 2         | S-400   | pralka    | 2199 |
| 10          | 2         | X-500   | telewizor | 1999 |
| 11          | 2         | X-600   | telewizor | 2599 |
| 12          | 2         | Z-700   | lodówka   | 2200 |
| 13          | 3         | U3000   | pralka    | 1550 |
| 14          | 3         | U3001   | pralka    | 2400 |
| 15          | 3         | G1000   | kuchenka  | 1999 |
| 16          | 3         | L100    | lodówka   | 2500 |
| 17          | 3         | L105    | lodówka   | 3150 |

<br>

2. ##### Wyświetla kolumny `model` i `cena` w tabeli produkty

```sql
SELECT model, cena
FROM produkty;
```

| model   | cena |
| :------ | :--- |
| EP-200  | 2500 |
| EP-200X | 2750 |
| EP-300  | 3199 |
| EE-1000 | 3999 |
| EE-2000 | 4499 |
| EX-550  | 2100 |
| EZ-550  | 3500 |
| S-300   | 2999 |
| S-400   | 2199 |
| X-500   | 1999 |
| X-600   | 2599 |
| Z-700   | 2200 |
| U3000   | 1550 |
| U3001   | 2400 |
| G1000   | 1999 |
| L100    | 2500 |
| L105    | 3150 |

<br>

3. ##### Wyświetla średnią cenę wszystkich produktów

```sql
SELECT avg(cena) AS "Średnia"
FROM produkty;
```

| Średnia   |
| :-------- |
| 2714.2353 |

<br>

4. ##### Wyświetla średnią cenę produktów producenta o identyfikatorze równym 1

```sql
SELECT avg(cena) AS "Średnia"
FROM produkty
WHERE producent = 1;
```

| Średnia   |
| :-------- |
| 3221.0000 |

<br>

5. ##### Wyświetla średnią cenę produktów producenta o identyfikatorze równym 2

```sql
SELECT avg(cena) AS "Średnia"
FROM produkty
WHERE producent = 2;
```

| Cena      |
| :-------- |
| 2399.2000 |

<br>

6. ##### Wyświetla sumę cen produktów producenta o identyfikatorze równym 1

```sql
SELECT sum(cena) AS "Suma"
FROM produkty
WHERE producent = 1;
```

| Suma  |
| :---- |
| 22547 |

<br>

7. ##### Wyświetla liczbę cen produktów producenta o identyfikatorze równym 1

```sql
SELECT count(cena) AS "Licznik"
FROM produkty
WHERE producent = 1;
```

| Licznik |
| :------ |
| 7       |

<br>

8. ##### Wyświetla liczbę wszystkich rekordów w tabeli "produkty"

```sql
SELECT count(*) AS "Liczni"
FROM produkty;
```

| Licznik |
| :------ |
| 17      |

<br>

9. ##### Wyświetla minimalną cenę produktu

```sql
SELECT min(cena) AS "Minimum"
FROM produkty;
```

| Minimum |
| :------ |
| 1550    |

<br>

10. ##### Wyświetla maksymalną cenę produktu

```sql
SELECT max(cena) AS "Maksimum"
FROM produkty;
```

| Maksimum |
| :------- |
| 4499     |

<br>

11. ##### Wyświetla średnią cenę produktów dla każdej kategorii

```sql
SELECT
    kategoria,
    avg(cena) AS "Średnia"
FROM produkty
GROUP BY kategoria;
```

| kategoria | Średnia   |
| :-------- | :-------- |
| pralka    | 2513.8571 |
| telewizor | 3274.0000 |
| kuchenka  | 2049.5000 |
| lodówka   | 2837.5000 |

>UWAGA!
>> GROUP BY jest podobne do DISTINCT

<br>

12. ##### Wyświetla średnią cenę produktów dla każdej kategorii i producenta, uporządkowane według producenta

```sql
SELECT
    producent,
    kategoria,
    avg(cena) AS "Średnia"
FROM produkty
GROUP BY kategoria, producent
ORDER BY producent;
```

| producent | kategoria | Średnia   |
| :-------- | :-------- | :-------- |
| 1         | pralka    | 2816.3333 |
| 1         | telewizor | 4249.0000 |
| 1         | kuchenka  | 2100.0000 |
| 1         | lodówka   | 3500.0000 |
| 2         | pralka    | 2599.0000 |
| 2         | telewizor | 2299.0000 |
| 2         | lodówka   | 2200.0000 |
| 3         | pralka    | 1975.0000 |
| 3         | kuchenka  | 1999.0000 |
| 3         | lodówka   | 2825.0000 |

<br>

13. ##### Wyświetla średnią cenę produktów dla każdej kategorii, ale tylko jeśli średnia cena przekracza 2500

```sql
SELECT
    kategoria,
    avg(cena) AS Średnia
FROM produkty
GROUP BY kategoria
HAVING  avg(cena) > 2500;
```

| kategoria | Średnia   |
| :-------- | :-------- |
| pralka    | 2513.8571 |
| telewizor | 3274.0000 |
| lodówka   | 2837.5000 |

<br>

14. ##### Wyświetla średnią cenę produktów dla każdej kategorii, ale tylko jeśli średnia cena przekracza 3000

```sql
SELECT
    kategoria,
    avg(cena) AS Średnia
FROM produkty
GROUP BY kategoria
HAVING  avg(cena) > 3000;
```

| kategoria | Średnia   |
| :-------- | :-------- |
| telewizor | 3274.0000 |
