<div align="center">
  <a href="https://www.youtube.com/@KoW">
    <img src="https://yt3.googleusercontent.com/-l3JZPYo50Dr9ugeHjMC51M2NKEamIqvXN4mmYKzJB_C2jdPb4UDX8-3s_eRziR_swVMJshN=s900-c-k-c0x00ffffff-no-rj" width="200px" alt="Kanał o Wszystkim" />
  </a>
<h1>Kanał o Wszystkim</h1>
</div>

## Informacje o Kursie

- **Nazwa Kursu:** Kurs SQL z kanału youtube Kanał o Wszystkim
- **Autor/Kreator:** Kanał o Wszystkim
- **Opis Kursu:** Kurs SQL dla początkujących obejmujący podstawowe zagadnienia związane z bazami danych MySQL.
- **Link do Kursu:** [Kurs SQL na YouTube](https://www.youtube.com/watch?v=BcZmEaX8u3w&list=PL6aekdNhY7DA1wcv-k2MtZxasDeGlre57&index=1)

## Struktura Kursu

1. [Kurs SQL - Łączenie z bazą MySQL, podstawowe zapytania SELECT](01.%20Kurs%20SQL%20-%20%C5%81%C4%85czenie%20z%20baz%C4%85%20MySQL%2C%20podstawowe%20zapytania%20SELECT/README.md)
2. [Kurs SQL - Sortowanie wyników, klauzula LIKE](02.%20Kurs%20SQL%20-%20Sortowanie%20wynik%C3%B3w%2C%20klauzula%20LIKE/README.md)
3. [Kurs SQL - LIMIT, DISTINCT oraz CONCAT](03.%20Kurs%20SQL%20-%20LIMIT%2C%20DISTINCT%20oraz%20CONCAT/README.md)
4. [Kurs SQL - funkcje grupujące, GROUP BY](04.%20Kurs%20SQL%20-%20funkcje%20grupuj%C4%85ce%2C%20GROUP%20BY/README.md)
5. [Kurs SQL - łączenie tabel (JOIN)](05.%20Kurs%20SQL%20-%20%C5%82%C4%85czenie%20tabel%20%28JOIN%29/README.md)
6. [Kurs SQL - dodawanie rekordów (INSERT)](06.%20Kurs%20SQL%20-%20dodawanie%20rekord%C3%B3w%20%28INSERT%29/README.md)
7. [Kurs SQL - modyfikowanie rekordów (UPDATE)](07.%20Kurs%20SQL%20-%20modyfikowanie%20rekord%C3%B3w%20%28UPDATE%29/README.md)
8. [Kurs SQL - usuwanie rekordów (DELETE)](08.%20Kurs%20SQL%20-%20usuwanie%20rekord%C3%B3w%20%28DELETE%29/README.md)
9. [Kurs SQL - podzapytania](09.%20Kurs%20SQL%20-%20podzapytania/README.md)
10. [Kurs SQL - instrukcje warunkowe (CASE)](10.%20Kurs%20SQL%20-%20instrukcje%20warunkowe%20%28CASE%29/README.md)

## Wymagania

- Instalacja serwera i bazy danych (np. XAMPP)
- Oprogramowanie do pracy z SQL (np. MySQL Workbench, DataGrip)
- [Polecenia SQL do wklejenia](https://pastebin.com/TQDJJ5ND)

## Instrukcje dla Uczestników

Kurs jest przeznaczony głównie dla osób początkujących, jest łatwy do zrozumienia i skupia się na podstawowych zagadnieniach związanych z językiem SQL.

## Dodatkowe Materiały

Dodatkowe materiały do pobrania znajdują się na stronie kursu:

- [Polecenia SQL do wklejenia](https://pastebin.com/TQDJJ5ND)

## Licencja

Cały kurs podstawowy jest udostępniony na platformie YouTube i można swobodnie korzystać z materiałów.

## Kontakt

- Komentarze pod filmami na [YouTube](https://www.youtube.com/c/KanalOWszystkim)
- E-mail: [adres-email@przykladowy.com](mailto:adres-email@przykladowy.com)
- Kanał na Discord: [Nazwa Kanału na Discord](https://discord.gg/your-discord-channel)
