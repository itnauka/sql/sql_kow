# 7. Kurs SQL - modyfikowanie rekordów (UPDATE)

<div align="center">
    <a href="https://www.youtube.com/watch?v=FJQF4-CfP8s&list=PL6aekdNhY7DA1wcv-k2MtZxasDeGlre57&index=7">
        <img src="https://i.ytimg.com/vi/FJQF4-CfP8s/hqdefault.jpg" alt="1. Kurs SQL" />
    </a>
</div>

<br>

---

### Przykładowe zapytania SQL

<br>

1. ##### Wyświetla `wszystkie` kolumny w tabeli produkty

```sql
SELECT *
FROM produkty;
```

| id_produktu | id_producenta | model    | kategoria | cena |
| :---------- | :------------ | :------- | :-------- | :--- |
| 1           | 1             | EP-200   | pralka    | 2500 |
| 2           | 1             | EP-200X  | pralka    | 2750 |
| 3           | 1             | EP-300   | pralka    | 3199 |
| 4           | 1             | EE-1000  | telewizor | 3999 |
| 5           | 1             | EE-2000  | telewizor | 4499 |
| 6           | 1             | EX-550   | kuchenka  | 2100 |
| 7           | 1             | EZ-550   | lodówka   | 3500 |
| 8           | 2             | S-300    | pralka    | 2999 |
| 9           | 2             | S-400    | pralka    | 2199 |
| 10          | 2             | X-500    | telewizor | 1999 |
| 11          | 2             | X-600    | telewizor | 2599 |
| 12          | 2             | Z-700    | lodówka   | 2200 |
| 13          | 3             | U3000    | pralka    | 1550 |
| 14          | 3             | U3001    | pralka    | 2400 |
| 15          | 3             | G1000    | kuchenka  | 1999 |
| 16          | 3             | L100     | lodówka   | 2500 |
| 17          | 3             | L105     | lodówka   | 3150 |
| 18          | 1             | EP-300X  | pralka    | 3250 |
| 19          | 2             | S-500    | pralka    | 2299 |
| 20          | 3             | G1500    | kuchenka  | 2499 |
| 21          | 1             | EP-300XS | pralka    | 3399 |

<br>

2. ##### Wyświetla `wszystkie` kolumny w tabeli produkty, gdzie id_produktu wynosi 18:

```sql
SELECT *
FROM produkty
WHERE id_produktu = 18;
```

| id_produktu | id_producenta | model   | kategoria | cena |
| :---------- | :------------ | :------ | :-------- | :--- |
| 18          | 1             | EP-300X | pralka    | 3250 |

<br>

3. ##### `Składnia` polecenia `UPDATE`:

```sql
UPDATE tabela
SET kolumna = nowa_wartość
WHERE kolumna = wartość;
```

<br>

4. ##### Aktualizuje wartość kolumny `model` w tabeli produkty, gdzie id_produktu wynosi 18:

```sql
UPDATE produkty
SET model = 'XYZ'
WHERE id_produktu = 18;
```

|             | PRZED   | PO    |
| :---------- | :------ | :---- |
| id_produktu | model   | model |
| 18          | EP-300X | XYZ   |

| id_produktu | id_producenta | model    | kategoria | cena |
| :---------- | :------------ | :------- | :-------- | :--- |
| 1           | 1             | EP-200   | pralka    | 2500 |
| 2           | 1             | EP-200X  | pralka    | 2750 |
| 3           | 1             | EP-300   | pralka    | 3199 |
| 4           | 1             | EE-1000  | telewizor | 3999 |
| 5           | 1             | EE-2000  | telewizor | 4499 |
| 6           | 1             | EX-550   | kuchenka  | 2100 |
| 7           | 1             | EZ-550   | lodówka   | 3500 |
| 8           | 2             | S-300    | pralka    | 2999 |
| 9           | 2             | S-400    | pralka    | 2199 |
| 10          | 2             | X-500    | telewizor | 1999 |
| 11          | 2             | X-600    | telewizor | 2599 |
| 12          | 2             | Z-700    | lodówka   | 2200 |
| 13          | 3             | U3000    | pralka    | 1550 |
| 14          | 3             | U3001    | pralka    | 2400 |
| 15          | 3             | G1000    | kuchenka  | 1999 |
| 16          | 3             | L100     | lodówka   | 2500 |
| 17          | 3             | L105     | lodówka   | 3150 |
| 18          | 1             | XYZ      | pralka    | 3250 |
| 19          | 2             | S-500    | pralka    | 2299 |
| 20          | 3             | G1500    | kuchenka  | 2499 |
| 21          | 1             | EP-300XS | pralka    | 3399 |

<br>

5. ##### Aktualizuje wartości kolumny `model` i `cena` w tabeli produkty, gdzie id_produktu wynosi 18:

```sql
UPDATE produkty
SET
model = 'EP-350X',
cena = 3200
WHERE id_produktu = 18;
```

`PRZED`
| id\_produktu | model | cena |
| :--- | :--- | :--- |
| 18 | EP-300X | 3250 |

<br>

`PO`
| id\_produktu | model | cena |
| :--- | :--- | :--- |
| 18 | EP-350X | 3200 |

<br>

| id\_produktu | id\_producenta | model | kategoria | cena |
| :--- | :--- | :--- | :--- | :--- |
| 1 | 1 | EP-200 | pralka | 2500 |
| 2 | 1 | EP-200X | pralka | 2750 |
| 3 | 1 | EP-300 | pralka | 3199 |
| 4 | 1 | EE-1000 | telewizor | 3999 |
| 5 | 1 | EE-2000 | telewizor | 4499 |
| 6 | 1 | EX-550 | kuchenka | 2100 |
| 7 | 1 | EZ-550 | lodówka | 3500 |
| 8 | 2 | S-300 | pralka | 2999 |
| 9 | 2 | S-400 | pralka | 2199 |
| 10 | 2 | X-500 | telewizor | 1999 |
| 11 | 2 | X-600 | telewizor | 2599 |
| 12 | 2 | Z-700 | lodówka | 2200 |
| 13 | 3 | U3000 | pralka | 1550 |
| 14 | 3 | U3001 | pralka | 2400 |
| 15 | 3 | G1000 | kuchenka | 1999 |
| 16 | 3 | L100 | lodówka | 2500 |
| 17 | 3 | L105 | lodówka | 3150 |
| 18 | 1 | EP-350X | pralka | 3200 |
| 19 | 2 | S-500 | pralka | 2299 |
| 20 | 3 | G1500 | kuchenka | 2499 |
| 21 | 1 | EP-300XS | pralka | 3399 |

<br>

6. ##### Wyświetla `wszystkie` kolumny w tabeli produkty, gdzie id_produktu jest równy 19 lub 20:

```sql
SELECT *
FROM produkty
WHERE id_produktu IN (19, 20);
```

| id\_produktu | id\_producenta | model | kategoria | cena |
| :--- | :--- | :--- | :--- | :--- |
| 19 | 2 | S-500 | pralka | 2299 |
| 20 | 3 | G1500 | kuchenka | 2499 |

<br>

7. ##### Obniża cenę o 100 dla produktów, gdzie id_produktu jest równy 19 lub 20:

```sql
UPDATE produkty
SET cena = (cena - 100)
WHERE id_produktu IN (19, 20);
```

| id\_produktu | id\_producenta | model | kategoria | cena |
| :--- | :--- | :--- | :--- | :--- |
| 19 | 2 | S-500 | pralka | 2199 |
| 20 | 3 | G1500 | kuchenka | 2399 |

<br>

8. ##### Obniża cenę o 50 dla produktów, gdzie id_produktu jest równy 19 lub 20:

```sql
UPDATE produkty
SET cena = (cena - 50)
WHERE id_produktu IN (19, 20);
```

| id\_produktu | id\_producenta | model | kategoria | cena |
| :--- | :--- | :--- | :--- | :--- |
| 19 | 2 | S-500 | pralka | 2149 |
| 20 | 3 | G1500 | kuchenka | 2349 |

