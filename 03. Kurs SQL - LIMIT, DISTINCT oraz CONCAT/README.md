# 3. Kurs SQL - LIMIT, DISTINCT oraz CONCAT
<div align="center">
    <a href="https://www.youtube.com/watch?v=wtrc1kL7CVY&list=PL6aekdNhY7DA1wcv-k2MtZxasDeGlre57&index=3">
        <img src="https://i.ytimg.com/vi/wtrc1kL7CVY/hqdefault.jpg" alt="1. Kurs SQL" />
    </a>
</div>

<br>

---

### Przykładowe zapytania SQL

<br>

1. ##### Wyświetla `wszystkie` kolumny w tabeli produkty

```sql
SELECT *
FROM produkty;
```

| id_produktu | producent | model   | kategoria | cena |
| :---------- | :-------- | :------ | :-------- | :--- |
| 1           | 1         | EP-200  | pralka    | 2500 |
| 2           | 1         | EP-200X | pralka    | 2750 |
| 3           | 1         | EP-300  | pralka    | 3199 |
| 4           | 1         | EE-1000 | telewizor | 3999 |
| 5           | 1         | EE-2000 | telewizor | 4499 |
| 6           | 1         | EX-550  | kuchenka  | 2100 |
| 7           | 1         | EZ-550  | lodówka   | 3500 |
| 8           | 2         | S-300   | pralka    | 2999 |
| 9           | 2         | S-400   | pralka    | 2199 |
| 10          | 2         | X-500   | telewizor | 1999 |
| 11          | 2         | X-600   | telewizor | 2599 |
| 12          | 2         | Z-700   | lodówka   | 2200 |
| 13          | 3         | U3000   | pralka    | 1550 |
| 14          | 3         | U3001   | pralka    | 2400 |
| 15          | 3         | G1000   | kuchenka  | 1999 |
| 16          | 3         | L100    | lodówka   | 2500 |
| 17          | 3         | L105    | lodówka   | 3150 |

<br>

2. ##### Wyświetla wszystkie kolumny z tabeli `produkty`, z ograniczeniem do pierwszych 5 wyników

```sql
SELECT *
FROM produkty
LIMIT 5;
```

| id\_produktu | producent | model | kategoria | cena |
| :--- | :--- | :--- | :--- | :--- |
| 1 | 1 | EP-200 | pralka | 2500 |
| 2 | 1 | EP-200X | pralka | 2750 |
| 3 | 1 | EP-300 | pralka | 3199 |
| 4 | 1 | EE-1000 | telewizor | 3999 |
| 5 | 1 | EE-2000 | telewizor | 4499 |

<br>

2. ##### Wyświetla `wszystkie` kolumny z tabeli produkty, posortowane malejąco według kolumny `cena`, z ograniczeniem do pierwszych 5 wyników

```sql
SELECT *
FROM produkty
ORDER BY cena DESC
LIMIT 5;
```

| id\_produktu | producent | model | kategoria | cena |
| :--- | :--- | :--- | :--- | :--- |
| 5 | 1 | EE-2000 | telewizor | 4499 |
| 4 | 1 | EE-1000 | telewizor | 3999 |
| 7 | 1 | EZ-550 | lodówka | 3500 |
| 3 | 1 | EP-300 | pralka | 3199 |
| 17 | 3 | L105 | lodówka | 3150 |

<br>

3. ##### Wyświetla kolumne `kategoria` w tabeli produkty

```sql
SELECT kategoria
FROM produkty;
```

| kategoria |
| :--- |
| pralka |
| pralka |
| pralka |
| telewizor |
| telewizor |
| kuchenka |
| lodówka |
| pralka |
| pralka |
| telewizor |
| telewizor |
| lodówka |
| pralka |
| pralka |
| kuchenka |
| lodówka |
| lodówka |

<br>

4. ##### Wyświetla `unikalne` wartości w kolumnie `kategoria` z tabeli produkty

```sql
SELECT DISTINCT kategoria
FROM produkty;
```

| kategoria |
| :--- |
| pralka |
| telewizor |
| kuchenka |
| lodówka |

<br>

5. ##### Wyświetla `unikalne` wartości dla kolumn `kategoria` i `producent` z tabeli produkty

```sql
SELECT DISTINCT kategoria, producent
FROM produkty;
```

| kategoria | producent |
| :--- | :--- |
| pralka | 1 |
| telewizor | 1 |
| kuchenka | 1 |
| lodówka | 1 |
| pralka | 2 |
| telewizor | 2 |
| lodówka | 2 |
| pralka | 3 |
| kuchenka | 3 |
| lodówka | 3 |

>UWAGA!
>>pralka - 1 różni się od pralka - 2
>>pomija pralka - 1, które się powtarzają

<br>

6. #####  Tworzy `nową` kolumnę, łącząc wartości z kolumn `kategoria` i `producent` dla każdego rekordu w tabeli "produkty".
```sql
SELECT
    CONCAT(
        produkty.kategoria,
        " ",
        produkty.producent)
FROM produkty;
```

| CONCAT\(<br/> produkty.kategoria,<br/> " ",<br/> produkty.producent\) |
| :--- |
| pralka 1 |
| pralka 1 |
| pralka 1 |
| telewizor 1 |
| telewizor 1 |
| kuchenka 1 |
| lodówka 1 |
| pralka 2 |
| pralka 2 |
| telewizor 2 |
| telewizor 2 |
| lodówka 2 |
| pralka 3 |
| pralka 3 |
| kuchenka 3 |
| lodówka 3 |
| lodówka 3 |

>UWAGA!
>>zła nazwa tabeli. Brak aliasu

<br>

7. ##### Tworzy nową kolumnę `produkt`, łącząc wartości z kolumn `kategoria` i `model`, oddzielone dwukropkiem, dla każdego rekordu w tabeli "produkty". Dodatkowo, zwraca cenę każdego produktu.
```sql
SELECT
    CONCAT(
        produkty.kategoria,
        " : ",
        produkty.model
    ) AS 'produkt',
    cena
FROM produkty;
```

| produkt | cena |
| :--- | :--- |
| pralka : EP-200 | 2500 |
| pralka : EP-200X | 2750 |
| pralka : EP-300 | 3199 |
| telewizor : EE-1000 | 3999 |
| telewizor : EE-2000 | 4499 |
| kuchenka : EX-550 | 2100 |
| lodówka : EZ-550 | 3500 |
| pralka : S-300 | 2999 |
| pralka : S-400 | 2199 |
| telewizor : X-500 | 1999 |
| telewizor : X-600 | 2599 |
| lodówka : Z-700 | 2200 |
| pralka : U3000 | 1550 |
| pralka : U3001 | 2400 |
| kuchenka : G1000 | 1999 |
| lodówka : L100 | 2500 |
| lodówka : L105 | 3150 |

<br>

8. ##### Tworzy nową kolumnę `produkt`, łącząc wartości z kolumn `kategoria` (przekształcone na wielkie litery) i `model`, oddzielone dwukropkiem, dla każdego rekordu w tabeli "produkty". Dodatkowo, zwraca cenę każdego produktu

```sql
SELECT
    CONCAT(
        upper(
            produkty.kategoria
        ),
            " : ",
            produkty.model
    ) AS 'produkt',
    cena
FROM produkty;
```

| produkt | cena |
| :--- | :--- |
| PRALKA : EP-200 | 2500 |
| PRALKA : EP-200X | 2750 |
| PRALKA : EP-300 | 3199 |
| TELEWIZOR : EE-1000 | 3999 |
| TELEWIZOR : EE-2000 | 4499 |
| KUCHENKA : EX-550 | 2100 |
| LODÓWKA : EZ-550 | 3500 |
| PRALKA : S-300 | 2999 |
| PRALKA : S-400 | 2199 |
| TELEWIZOR : X-500 | 1999 |
| TELEWIZOR : X-600 | 2599 |
| LODÓWKA : Z-700 | 2200 |
| PRALKA : U3000 | 1550 |
| PRALKA : U3001 | 2400 |
| KUCHENKA : G1000 | 1999 |
| LODÓWKA : L100 | 2500 |
| LODÓWKA : L105 | 3150 |

<br>

9. ##### Tworzy nową kolumnę `produkt`, łącząc wartości z kolumn `kategoria` (przekształcone na wielkie litery) i `model`, `cena`, oraz dodaje informację o cenie w złotych, dla każdego rekordu w tabeli "produkty". Dodatkowo, ogranicza wyniki do pierwszych 5 rekordów.

```sql
SELECT
    CONCAT(
        upper(
            produkty.kategoria
        ),
        " : ",
        produkty.model,
        ' cena: ',
        cena,
        ' złotych'
    ) AS 'produkt'
FROM produkty
LIMIT 5;
```

| produkt |
| :--- |
| PRALKA : EP-200 cena: 2500 złotych |
| PRALKA : EP-200X cena: 2750 złotych |
| PRALKA : EP-300 cena: 3199 złotych |
| TELEWIZOR : EE-1000 cena: 3999 złotych |
| TELEWIZOR : EE-2000 cena: 4499 złotych |


