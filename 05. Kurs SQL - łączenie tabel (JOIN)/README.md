# 5. Kurs SQL - łączenie tabel (JOIN)

<div align="center">
    <a href="https://www.youtube.com/watch?v=Ttg008YYdro&list=PL6aekdNhY7DA1wcv-k2MtZxasDeGlre57&index=6">
        <img src="https://i.ytimg.com/vi/Ttg008YYdro/hqdefault.jpg" alt="1. Kurs SQL" />
    </a>
</div>

<br>

---

### Przykładowe zapytania SQL

<br>

1. ##### Wyświetla `wszystkie` kolumny w tabeli produkty

```sql
SELECT *
FROM produkty;
```

| id_produktu | producent | model   | kategoria | cena |
| :---------- | :-------- | :------ | :-------- | :--- |
| 1           | 1         | EP-200  | pralka    | 2500 |
| 2           | 1         | EP-200X | pralka    | 2750 |
| 3           | 1         | EP-300  | pralka    | 3199 |
| 4           | 1         | EE-1000 | telewizor | 3999 |
| 5           | 1         | EE-2000 | telewizor | 4499 |
| 6           | 1         | EX-550  | kuchenka  | 2100 |
| 7           | 1         | EZ-550  | lodówka   | 3500 |
| 8           | 2         | S-300   | pralka    | 2999 |
| 9           | 2         | S-400   | pralka    | 2199 |
| 10          | 2         | X-500   | telewizor | 1999 |
| 11          | 2         | X-600   | telewizor | 2599 |
| 12          | 2         | Z-700   | lodówka   | 2200 |
| 13          | 3         | U3000   | pralka    | 1550 |
| 14          | 3         | U3001   | pralka    | 2400 |
| 15          | 3         | G1000   | kuchenka  | 1999 |
| 16          | 3         | L100    | lodówka   | 2500 |
| 17          | 3         | L105    | lodówka   | 3150 |

<br>

2. ##### Wyświetla `wszystkie` kolumny w tabeli "producenci"

```sql
SELECT *
FROM producenci;
```

| id_producenta | nazwa   | data_powstania | kraj             |
| :------------ | :------ | :------------- | :--------------- |
| 1             | Samsung | 1995-12-01     | Korea Poludniowa |
| 2             | LG      | 1991-09-11     | Korea Poludniowa |
| 3             | Amica   | 1997-07-24     | Polska           |

<br>

3. ##### Wyświetla kolumny `model` z tabeli "produkty" i `nazwa` z tabeli "producenci" bez warunku

```sql
SELECT
    produkty.model,
    producenci.nazwa
FROM produkty, producenci;
```

| model   | nazwa   |
| :------ | :------ |
| EE-1000 | Samsung |
| EE-1000 | LG      |
| EE-1000 | Amica   |
| EE-2000 | Samsung |
| EE-2000 | LG      |
| EE-2000 | Amica   |
| EP-200  | Samsung |
| EP-200  | LG      |
| EP-200  | Amica   |
| EP-200X | Samsung |
| EP-200X | LG      |
| EP-200X | Amica   |
| EP-300  | Samsung |
| EP-300  | LG      |
| EP-300  | Amica   |
| EX-550  | Samsung |
| EX-550  | LG      |
| EX-550  | Amica   |
| EZ-550  | Samsung |
| EZ-550  | LG      |
| EZ-550  | Amica   |
| G1000   | Samsung |
| G1000   | LG      |
| G1000   | Amica   |
| L100    | Samsung |
| L100    | LG      |
| L100    | Amica   |
| L105    | Samsung |
| L105    | LG      |
| L105    | Amica   |
| S-300   | Samsung |
| S-300   | LG      |
| S-300   | Amica   |
| S-400   | Samsung |
| S-400   | LG      |
| S-400   | Amica   |
| U3000   | Samsung |
| U3000   | LG      |
| U3000   | Amica   |
| U3001   | Samsung |
| U3001   | LG      |
| U3001   | Amica   |
| X-500   | Samsung |
| X-500   | LG      |
| X-500   | Amica   |
| X-600   | Samsung |
| X-600   | LG      |
| X-600   | Amica   |
| Z-700   | Samsung |
| Z-700   | LG      |
| Z-700   | Amica   |

> UWAGA!
>
> > **błędne powiązanie:**  
> > każdy wiersz z jednej tabel z każdym wierszem z drugiej tabeli

<br>

4. ##### Wyświetla kolumny `model` z tabeli "produkty" i `nazwa` z tabeli "producenci" dla rekordów, gdzie `id_produktu` w tabeli "produkty" jest równy `id_producenta` w tabeli "producenci"

```sql
SELECT
    produkty.model,
    producenci.nazwa
FROM produkty, producenci
WHERE produkty.producent = producenci.id_producenta;
```

| model   | nazwa   |
| :------ | :------ |
| U3000   | Amica   |
| U3001   | Amica   |
| G1000   | Amica   |
| L100    | Amica   |
| L105    | Amica   |
| S-300   | LG      |
| S-400   | LG      |
| X-500   | LG      |
| X-600   | LG      |
| Z-700   | LG      |
| EP-200  | Samsung |
| EP-200X | Samsung |
| EP-300  | Samsung |
| EE-1000 | Samsung |
| EE-2000 | Samsung |
| EX-550  | Samsung |
| EZ-550  | Samsung |

<br>

5. ##### Wyświetla kolumny `model` z tabeli "produkty" i `nazwa` z tabeli "producenci" dla rekordów, gdzie `id_produktu` w tabeli "produkty" jest równy `id_producent`a w tabeli "producenci" (z użyciem INNER JOIN)

```sql
SELECT
    produkty.model,
    producenci.nazwa
FROM produkty
    INNER JOIN sklep.producenci ON producenci.id_producenta = produkty.producent;
```

| model   | nazwa   |
| :------ | :------ |
| U3000   | Amica   |
| U3001   | Amica   |
| G1000   | Amica   |
| L100    | Amica   |
| L105    | Amica   |
| S-300   | LG      |
| S-400   | LG      |
| X-500   | LG      |
| X-600   | LG      |
| Z-700   | LG      |
| EP-200  | Samsung |
| EP-200X | Samsung |
| EP-300  | Samsung |
| EE-1000 | Samsung |
| EE-2000 | Samsung |
| EX-550  | Samsung |
| EZ-550  | Samsung |

<br>

6. ##### Wyświetla kolumny `model` z tabeli "produkty" i `nazwa` z tabeli "producenci" dla rekordów, gdzie `id_produktu` w tabeli "produkty" jest równy `id_producenta` w tabeli "producenci" (z użyciem LEFT JOIN)

```sql
SELECT
    produkty.model,
    producenci.nazwa
FROM produkty
    LEFT JOIN sklep.producenci ON producenci.id_producenta = produkty.producent;
```

| model   | nazwa   |
| :------ | :------ |
| EP-200  | Samsung |
| EP-200X | Samsung |
| EP-300  | Samsung |
| EE-1000 | Samsung |
| EE-2000 | Samsung |
| EX-550  | Samsung |
| EZ-550  | Samsung |
| S-300   | LG      |
| S-400   | LG      |
| X-500   | LG      |
| X-600   | LG      |
| Z-700   | LG      |
| U3000   | Amica   |
| U3001   | Amica   |
| G1000   | Amica   |
| L100    | Amica   |
| L105    | Amica   |

<br>

7. ##### Wyświetla kolumny `model` z tabeli "produkty" i `nazwa` z tabeli "producenci" dla rekordów, gdzie `id_produktu` w tabeli "produkty" jest równy `id_producenta` w tabeli "producenci" (z użyciem RIGHT JOIN)

```sql
SELECT
    produkty.model,
    producenci.nazwa
FROM produkty
    RIGHT JOIN sklep.producenci ON producenci.id_producenta = produkty.id_producenta;
```

| model   | nazwa   |
| :------ | :------ |
| U3000   | Amica   |
| U3001   | Amica   |
| G1000   | Amica   |
| L100    | Amica   |
| L105    | Amica   |
| S-300   | LG      |
| S-400   | LG      |
| X-500   | LG      |
| X-600   | LG      |
| Z-700   | LG      |
| EP-200  | Samsung |
| EP-200X | Samsung |
| EP-300  | Samsung |
| EE-1000 | Samsung |
| EE-2000 | Samsung |
| EX-550  | Samsung |
| EZ-550  | Samsung |

<br>

8. ##### Zmienia nazwę kolumny `producent` na `id_producenta` w tabeli "produkty"

```sql
ALTER TABLE produkty CHANGE COLUMN producent id_producenta int;
```

<br>

9. ##### Wyświetla kolumny `model` z tabeli produkty i `nazwa` z tabeli "producenci" dla rekordów, gdzie kolumny o tej samej nazwie są równe (Natural Join)

```sql
SELECT
    produkty.model,
    producenci.nazwa
FROM produkty
    NATURAL JOIN sklep.producenci;
```

| model   | nazwa   |
| :------ | :------ |
| U3000   | Amica   |
| U3001   | Amica   |
| G1000   | Amica   |
| L100    | Amica   |
| L105    | Amica   |
| S-300   | LG      |
| S-400   | LG      |
| X-500   | LG      |
| X-600   | LG      |
| Z-700   | LG      |
| EP-200  | Samsung |
| EP-200X | Samsung |
| EP-300  | Samsung |
| EE-1000 | Samsung |
| EE-2000 | Samsung |
| EX-550  | Samsung |
| EZ-550  | Samsung |

> UWAGA!
>> Nazwa z tabeli pierwszej musi być taka sama jak nazwa z tabeli drugiej