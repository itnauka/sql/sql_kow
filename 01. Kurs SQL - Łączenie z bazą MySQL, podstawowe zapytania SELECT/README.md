# 1. Kurs SQL - Łączenie z bazą MySQL, podstawowe zapytania SELECT

<div align="center">
    <a href="https://www.youtube.com/watch?v=BcZmEaX8u3w&list=PL6aekdNhY7DA1wcv-k2MtZxasDeGlre57&index=4">
        <img src="https://i.ytimg.com/vi/BcZmEaX8u3w/hqdefault.jpg" alt="1. Kurs SQL" />
    </a>
</div>

<br>

---

### Przykładowe zapytania SQL

<br>

1. ##### Wyświetla `wszystkie` kolumny w tabeli producenci

```sql
SELECT *
FROM producenci;
```

| id_producenta | nazwa   | data_powstania | kraj             |
| :------------ | :------ | :------------- | :--------------- |
| 1             | Samsung | 1995-12-01     | Korea Poludniowa |
| 2             | LG      | 1991-09-11     | Korea Poludniowa |
| 3             | Amica   | 1997-07-24     | Polska           |

<br>

2. ##### Wyświetla `wszystkie` kolumny w tabeli produkty

```sql
SELECT *
FROM produkty;
```

| id_produktu | producent | model   | kategoria | cena |
| :---------- | :-------- | :------ | :-------- | :--- |
| 1           | 1         | EP-200  | pralka    | 2500 |
| 2           | 1         | EP-200X | pralka    | 2750 |
| 3           | 1         | EP-300  | pralka    | 3199 |
| 4           | 1         | EE-1000 | telewizor | 3999 |
| 5           | 1         | EE-2000 | telewizor | 4499 |
| 6           | 1         | EX-550  | kuchenka  | 2100 |
| 7           | 1         | EZ-550  | lodówka   | 3500 |
| 8           | 2         | S-300   | pralka    | 2999 |
| 9           | 2         | S-400   | pralka    | 2199 |
| 10          | 2         | X-500   | telewizor | 1999 |
| 11          | 2         | X-600   | telewizor | 2599 |
| 12          | 2         | Z-700   | lodówka   | 2200 |
| 13          | 3         | U3000   | pralka    | 1550 |
| 14          | 3         | U3001   | pralka    | 2400 |
| 15          | 3         | G1000   | kuchenka  | 1999 |
| 16          | 3         | L100    | lodówka   | 2500 |
| 17          | 3         | L105    | lodówka   | 3150 |

<br>

3. ##### Wyświetla kolumnę `nazwa` w tabeli producenci

```sql
SELECT producenci.nazwa
FROM producenci;
```

| nazwa   |
| :------ |
| Amica   |
| LG      |
| Samsung |

<br>

4. ##### Wyświetla kolumny `nazwa`, `kraj` w tabeli producenci

```sql
SELECT
   producenci.nazwa,
   producenci.kraj
FROM producenci;
```

| nazwa   | kraj             |
| :------ | :--------------- |
| Samsung | Korea Poludniowa |
| LG      | Korea Poludniowa |
| Amica   | Polska           |

<br>

5. ##### Wyświetla `wszystkie` kolumny w tabeli produkty gdzie producent = 1

```sql
SELECT *
FROM produkty
WHERE producent = 1;
```

| id_produktu | producent | model   | kategoria | cena |
| :---------- | :-------- | :------ | :-------- | :--- |
| 1           | 1         | EP-200  | pralka    | 2500 |
| 2           | 1         | EP-200X | pralka    | 2750 |
| 3           | 1         | EP-300  | pralka    | 3199 |
| 4           | 1         | EE-1000 | telewizor | 3999 |
| 5           | 1         | EE-2000 | telewizor | 4499 |
| 6           | 1         | EX-550  | kuchenka  | 2100 |
| 7           | 1         | EZ-550  | lodówka   | 3500 |

<br>

6. ##### Wyświetla `wszystkie` kolumny w tabeli produkty gdzie producent = 1 lub producent jest różny od 3

```sql
SELECT *
FROM produkty
WHERE producent = 1 or producent != 3;
```

| id_produktu | producent | model   | kategoria | cena |
| :---------- | :-------- | :------ | :-------- | :--- |
| 1           | 1         | EP-200  | pralka    | 2500 |
| 2           | 1         | EP-200X | pralka    | 2750 |
| 3           | 1         | EP-300  | pralka    | 3199 |
| 4           | 1         | EE-1000 | telewizor | 3999 |
| 5           | 1         | EE-2000 | telewizor | 4499 |
| 6           | 1         | EX-550  | kuchenka  | 2100 |
| 7           | 1         | EZ-550  | lodówka   | 3500 |
| 8           | 2         | S-300   | pralka    | 2999 |
| 9           | 2         | S-400   | pralka    | 2199 |
| 10          | 2         | X-500   | telewizor | 1999 |
| 11          | 2         | X-600   | telewizor | 2599 |
| 12          | 2         | Z-700   | lodówka   | 2200 |

<br>

7. ##### Wyświetla `wszystkie` kolumny w tabeli produkty gdzie producent = 1 lub producent jest różny od 3

```sql
SELECT *
FROM produkty
WHERE producent = 1 or producent <> 3;
```

| id_produktu | producent | model   | kategoria | cena |
| :---------- | :-------- | :------ | :-------- | :--- |
| 1           | 1         | EP-200  | pralka    | 2500 |
| 2           | 1         | EP-200X | pralka    | 2750 |
| 3           | 1         | EP-300  | pralka    | 3199 |
| 4           | 1         | EE-1000 | telewizor | 3999 |
| 5           | 1         | EE-2000 | telewizor | 4499 |
| 6           | 1         | EX-550  | kuchenka  | 2100 |
| 7           | 1         | EZ-550  | lodówka   | 3500 |
| 8           | 2         | S-300   | pralka    | 2999 |
| 9           | 2         | S-400   | pralka    | 2199 |
| 10          | 2         | X-500   | telewizor | 1999 |
| 11          | 2         | X-600   | telewizor | 2599 |
| 12          | 2         | Z-700   | lodówka   | 2200 |

<br>

8. ##### Wyświetla `wszystkie` kolumny w tabeli produkty gdzie producent jest w zbiorze 1 lub 2

```sql
SELECT *
FROM produkty
WHERE producent IN(1, 2);
```

| id_produktu | producent | model   | kategoria | cena |
| :---------- | :-------- | :------ | :-------- | :--- |
| 1           | 1         | EP-200  | pralka    | 2500 |
| 2           | 1         | EP-200X | pralka    | 2750 |
| 3           | 1         | EP-300  | pralka    | 3199 |
| 4           | 1         | EE-1000 | telewizor | 3999 |
| 5           | 1         | EE-2000 | telewizor | 4499 |
| 6           | 1         | EX-550  | kuchenka  | 2100 |
| 7           | 1         | EZ-550  | lodówka   | 3500 |
| 8           | 2         | S-300   | pralka    | 2999 |
| 9           | 2         | S-400   | pralka    | 2199 |
| 10          | 2         | X-500   | telewizor | 1999 |
| 11          | 2         | X-600   | telewizor | 2599 |
| 12          | 2         | Z-700   | lodówka   | 2200 |

<br>

9. ##### Wyświetla `wszystkie` kolumny w tabeli produkty gdzie producent jest w zbiorze 1 lub 2 lub 3

```sql
SELECT *
FROM produkty
WHERE producent IN(1, 3, 2);
```

| id_produktu | producent | model   | kategoria | cena |
| :---------- | :-------- | :------ | :-------- | :--- |
| 1           | 1         | EP-200  | pralka    | 2500 |
| 2           | 1         | EP-200X | pralka    | 2750 |
| 3           | 1         | EP-300  | pralka    | 3199 |
| 4           | 1         | EE-1000 | telewizor | 3999 |
| 5           | 1         | EE-2000 | telewizor | 4499 |
| 6           | 1         | EX-550  | kuchenka  | 2100 |
| 7           | 1         | EZ-550  | lodówka   | 3500 |
| 8           | 2         | S-300   | pralka    | 2999 |
| 9           | 2         | S-400   | pralka    | 2199 |
| 10          | 2         | X-500   | telewizor | 1999 |
| 11          | 2         | X-600   | telewizor | 2599 |
| 12          | 2         | Z-700   | lodówka   | 2200 |
| 13          | 3         | U3000   | pralka    | 1550 |
| 14          | 3         | U3001   | pralka    | 2400 |
| 15          | 3         | G1000   | kuchenka  | 1999 |
| 16          | 3         | L100    | lodówka   | 2500 |
| 17          | 3         | L105    | lodówka   | 3150 |

<br>

10. ##### Wyświetla `wszystkie` kolumny w tabeli produkty gdzie kategoria = pralka

```sql
SELECT *
FROM produkty
WHERE kategoria = 'pralka';
```

| id_produktu | producent | model   | kategoria | cena |
| :---------- | :-------- | :------ | :-------- | :--- |
| 1           | 1         | EP-200  | pralka    | 2500 |
| 2           | 1         | EP-200X | pralka    | 2750 |
| 3           | 1         | EP-300  | pralka    | 3199 |
| 8           | 2         | S-300   | pralka    | 2999 |
| 9           | 2         | S-400   | pralka    | 2199 |
| 13          | 3         | U3000   | pralka    | 1550 |
| 14          | 3         | U3001   | pralka    | 2400 |

<br>

11. ##### Wyświetla `wszystkie` kolumny w tabeli produkty gdzie kategoria = pralka i jednocześnie producent = 1

```sql
SELECT *
FROM produkty
WHERE kategoria = 'pralka' AND producent = 1;
```

| id_produktu | producent | model   | kategoria | cena |
| :---------- | :-------- | :------ | :-------- | :--- |
| 1           | 1         | EP-200  | pralka    | 2500 |
| 2           | 1         | EP-200X | pralka    | 2750 |
| 3           | 1         | EP-300  | pralka    | 3199 |

<br>

12. ##### Wyświetla `wszystkie` kolumny w tabeli produkty gdzie kategoria = pralka i jednocześnie cena jest mniejsza od 2500

```sql
SELECT *
FROM produkty
WHERE kategoria = 'pralka' AND cena < 2500;
```

| id_produktu | producent | model | kategoria | cena |
| :---------- | :-------- | :---- | :-------- | :--- |
| 9           | 2         | S-400 | pralka    | 2199 |
| 13          | 3         | U3000 | pralka    | 1550 |
| 14          | 3         | U3001 | pralka    | 2400 |

<br>

13. ##### Wyświetla `wszystkie` kolumny w tabeli produkty gdzie kategoria = pralka i jednocześnie cena jest większa od 2500

```sql
SELECT *
FROM produkty
WHERE kategoria = 'pralka' AND cena > 2500;
```

| id_produktu | producent | model   | kategoria | cena |
| :---------- | :-------- | :------ | :-------- | :--- |
| 2           | 1         | EP-200X | pralka    | 2750 |
| 3           | 1         | EP-300  | pralka    | 3199 |
| 8           | 2         | S-300   | pralka    | 2999 |

<br>

14. ##### Wyświetla `wszystkie` kolumny w tabeli produkty gdzie kategoria = pralka i jednocześnie cena jest mniejsza bądź równa 2500

```sql
SELECT *
FROM produkty
WHERE kategoria = 'pralka' AND cena <= 2500;
```

| id_produktu | producent | model  | kategoria | cena |
| :---------- | :-------- | :----- | :-------- | :--- |
| 1           | 1         | EP-200 | pralka    | 2500 |
| 9           | 2         | S-400  | pralka    | 2199 |
| 13          | 3         | U3000  | pralka    | 1550 |
| 14          | 3         | U3001  | pralka    | 2400 |

<br>

15. ##### Wyświetla `wszystkie` kolumny w tabeli produkty gdzie kategoria = pralka i jednocześnie cena jest większa bądź równa 2500

```sql
SELECT *
FROM produkty
WHERE kategoria = 'pralka' AND cena >= 2500;
```

| id_produktu | producent | model   | kategoria | cena |
| :---------- | :-------- | :------ | :-------- | :--- |
| 1           | 1         | EP-200  | pralka    | 2500 |
| 2           | 1         | EP-200X | pralka    | 2750 |
| 3           | 1         | EP-300  | pralka    | 3199 |
| 8           | 2         | S-300   | pralka    | 2999 |

<br>

16. ##### Wyświetla kolumny `model` jako `nazwa`, `kategoria`, `cena` jako \$\$\$\$\$ w tabeli produkty gdzie kategoria = pralka i jednocześnie cena jest większa bądź równa 2500

```sql
SELECT
    produkty.model AS 'nazwa',
    produkty.kategoria,
    produkty.cena AS '$$$$$'
FROM produkty
WHERE kategoria = 'pralka' AND produkty.cena >= 2500;
```

| nazwa   | kategoria | \$\$\$\$\$ |
| :------ | :-------- | :--------- |
| EP-200  | pralka    | 2500       |
| EP-200X | pralka    | 2750       |
| EP-300  | pralka    | 3199       |
| S-300   | pralka    | 2999       |
