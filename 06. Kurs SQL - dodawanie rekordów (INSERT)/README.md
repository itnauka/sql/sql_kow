# 6. Kurs SQL - dodawanie rekordów (INSERT)

<div align="center">
    <a href="https://www.youtube.com/watch?v=dsGJyUHwgZc&list=PL6aekdNhY7DA1wcv-k2MtZxasDeGlre57&index=6">
        <img src="https://i.ytimg.com/vi/dsGJyUHwgZc/hqdefault.jpg" alt="1. Kurs SQL" />
    </a>
</div>

<br>

---

### Przykładowe zapytania SQL

<br>

1. ##### Wyświetla `wszystkie` kolumny w tabeli produkty

```sql
SELECT *
FROM produkty;
```

| id_produktu | producent | model   | kategoria | cena |
| :---------- | :-------- | :------ | :-------- | :--- |
| 1           | 1         | EP-200  | pralka    | 2500 |
| 2           | 1         | EP-200X | pralka    | 2750 |
| 3           | 1         | EP-300  | pralka    | 3199 |
| 4           | 1         | EE-1000 | telewizor | 3999 |
| 5           | 1         | EE-2000 | telewizor | 4499 |
| 6           | 1         | EX-550  | kuchenka  | 2100 |
| 7           | 1         | EZ-550  | lodówka   | 3500 |
| 8           | 2         | S-300   | pralka    | 2999 |
| 9           | 2         | S-400   | pralka    | 2199 |
| 10          | 2         | X-500   | telewizor | 1999 |
| 11          | 2         | X-600   | telewizor | 2599 |
| 12          | 2         | Z-700   | lodówka   | 2200 |
| 13          | 3         | U3000   | pralka    | 1550 |
| 14          | 3         | U3001   | pralka    | 2400 |
| 15          | 3         | G1000   | kuchenka  | 1999 |
| 16          | 3         | L100    | lodówka   | 2500 |
| 17          | 3         | L105    | lodówka   | 3150 |

<br>

2. ##### Wstawia nowy rekord do tabeli produkty z określonymi danymi:

```sql
INSERT INTO produkty(id_produktu, id_producenta, model, kategoria, cena)
VALUES (NULL, 1, 'EP-300X', 'pralka', 3250);
```

| id\_produktu | id\_producenta | model | kategoria | cena |
| :--- | :--- | :--- | :--- | :--- |
| 1 | 1 | EP-200 | pralka | 2500 |
| 2 | 1 | EP-200X | pralka | 2750 |
| 3 | 1 | EP-300 | pralka | 3199 |
| 4 | 1 | EE-1000 | telewizor | 3999 |
| 5 | 1 | EE-2000 | telewizor | 4499 |
| 6 | 1 | EX-550 | kuchenka | 2100 |
| 7 | 1 | EZ-550 | lodówka | 3500 |
| 8 | 2 | S-300 | pralka | 2999 |
| 9 | 2 | S-400 | pralka | 2199 |
| 10 | 2 | X-500 | telewizor | 1999 |
| 11 | 2 | X-600 | telewizor | 2599 |
| 12 | 2 | Z-700 | lodówka | 2200 |
| 13 | 3 | U3000 | pralka | 1550 |
| 14 | 3 | U3001 | pralka | 2400 |
| 15 | 3 | G1000 | kuchenka | 1999 |
| 16 | 3 | L100 | lodówka | 2500 |
| 17 | 3 | L105 | lodówka | 3150 |
| 18 | 1 | EP-300X | pralka | 3250 |

<br>

3. ##### Wyświetla wszystkie kolumny w tabeli produkty, sortuje po id_produktu malejąco i ogranicza wyniki do jednego:
```sql
SELECT *
FROM produkty
ORDER BY id_produktu DESC LIMIT 1;
```

| id\_produktu | id\_producenta | model | kategoria | cena |
| :--- | :--- | :--- | :--- | :--- |
| 18 | 1 | EP-300X | pralka | 3250 |

<br>

4. ##### Wstawia dwa nowe rekordy do tabeli produkty z określonymi danymi:
```sql
INSERT INTO produkty(id_producenta, model, kategoria, cena)
VALUES
    (2, 'S-500', 'pralka', 2299),
    (3, 'G1500', 'kuchenka', 2499);
```

| id\_produktu | id\_producenta | model | kategoria | cena |
| :--- | :--- | :--- | :--- | :--- |
| 1 | 1 | EP-200 | pralka | 2500 |
| 2 | 1 | EP-200X | pralka | 2750 |
| 3 | 1 | EP-300 | pralka | 3199 |
| 4 | 1 | EE-1000 | telewizor | 3999 |
| 5 | 1 | EE-2000 | telewizor | 4499 |
| 6 | 1 | EX-550 | kuchenka | 2100 |
| 7 | 1 | EZ-550 | lodówka | 3500 |
| 8 | 2 | S-300 | pralka | 2999 |
| 9 | 2 | S-400 | pralka | 2199 |
| 10 | 2 | X-500 | telewizor | 1999 |
| 11 | 2 | X-600 | telewizor | 2599 |
| 12 | 2 | Z-700 | lodówka | 2200 |
| 13 | 3 | U3000 | pralka | 1550 |
| 14 | 3 | U3001 | pralka | 2400 |
| 15 | 3 | G1000 | kuchenka | 1999 |
| 16 | 3 | L100 | lodówka | 2500 |
| 17 | 3 | L105 | lodówka | 3150 |
| 18 | 1 | EP-300X | pralka | 3250 |
| 19 | 2 | S-500 | pralka | 2299 |
| 20 | 3 | G1500 | kuchenka | 2499 |

<br>

5. ##### Wstawia nowy rekord do tabeli produkty z pełnym zestawem danych:
```sql
INSERT INTO produkty
VALUES (NULL, 1, 'EP-300XS', 'pralka', 3399);
```

| id\_produktu | id\_producenta | model | kategoria | cena |
| :--- | :--- | :--- | :--- | :--- |
| 1 | 1 | EP-200 | pralka | 2500 |
| 2 | 1 | EP-200X | pralka | 2750 |
| 3 | 1 | EP-300 | pralka | 3199 |
| 4 | 1 | EE-1000 | telewizor | 3999 |
| 5 | 1 | EE-2000 | telewizor | 4499 |
| 6 | 1 | EX-550 | kuchenka | 2100 |
| 7 | 1 | EZ-550 | lodówka | 3500 |
| 8 | 2 | S-300 | pralka | 2999 |
| 9 | 2 | S-400 | pralka | 2199 |
| 10 | 2 | X-500 | telewizor | 1999 |
| 11 | 2 | X-600 | telewizor | 2599 |
| 12 | 2 | Z-700 | lodówka | 2200 |
| 13 | 3 | U3000 | pralka | 1550 |
| 14 | 3 | U3001 | pralka | 2400 |
| 15 | 3 | G1000 | kuchenka | 1999 |
| 16 | 3 | L100 | lodówka | 2500 |
| 17 | 3 | L105 | lodówka | 3150 |
| 18 | 1 | EP-300X | pralka | 3250 |
| 19 | 2 | S-500 | pralka | 2299 |
| 20 | 3 | G1500 | kuchenka | 2499 |
| 21 | 1 | EP-300XS | pralka | 3399 |

> UWAGA!
> > Jeżeli nie podajemy nazw kolumn po poleceniu INSERT INTO to należy traktować jakby były wypisane wszystkie i po poleceni VALUES podać wszyskie wartości