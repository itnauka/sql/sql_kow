# 9. Kurs SQL - podzapytania

<div align="center">
    <a href="https://www.youtube.com/watch?v=kQfJ_tLxEwc&list=PL6aekdNhY7DA1wcv-k2MtZxasDeGlre57&index=9">
        <img src="https://i.ytimg.com/vi/kQfJ_tLxEwc/hqdefault.jpg" alt="1. Kurs SQL" />
    </a>
</div>

<br>

---

### Przykładowe zapytania SQL

<br>

1. ##### Wyświetla `wszystkie` kolumny w tabeli produkty

```sql
SELECT *
FROM produkty;
```

| id_produktu | producent | model   | kategoria | cena |
| :---------- | :-------- | :------ | :-------- | :--- |
| 1           | 1         | EP-200  | pralka    | 2500 |
| 2           | 1         | EP-200X | pralka    | 2750 |
| 3           | 1         | EP-300  | pralka    | 3199 |
| 4           | 1         | EE-1000 | telewizor | 3999 |
| 5           | 1         | EE-2000 | telewizor | 4499 |
| 6           | 1         | EX-550  | kuchenka  | 2100 |
| 7           | 1         | EZ-550  | lodówka   | 3500 |
| 8           | 2         | S-300   | pralka    | 2999 |
| 9           | 2         | S-400   | pralka    | 2199 |
| 10          | 2         | X-500   | telewizor | 1999 |
| 11          | 2         | X-600   | telewizor | 2599 |
| 12          | 2         | Z-700   | lodówka   | 2200 |
| 13          | 3         | U3000   | pralka    | 1550 |
| 14          | 3         | U3001   | pralka    | 2400 |
| 15          | 3         | G1000   | kuchenka  | 1999 |
| 16          | 3         | L100    | lodówka   | 2500 |
| 17          | 3         | L105    | lodówka   | 3150 |

<br>

2. ##### Wyświetla średnią cenę wszystkich produktów

```sql
SELECT
    avg(cena) AS 'Średnia'
FROM produkty;
```

| Średnia   |
| :-------- |
| 2714.2353 |

<br>

3. ##### Wybiera produkty, które są droższe niż średnia cena

```sql
SELECT
    id_produktu,
    model,
    cena
FROM produkty
WHERE cena > 2714.2353
ORDER BY cena;
```

| id_produktu | model   | cena |
| :---------- | :------ | :--- |
| 2           | EP-200X | 2750 |
| 8           | S-300   | 2999 |
| 17          | L105    | 3150 |
| 3           | EP-300  | 3199 |
| 7           | EZ-550  | 3500 |
| 4           | EE-1000 | 3999 |
| 5           | EE-2000 | 4499 |

<br>

4. ##### Wybiera produkty, które są droższe niż średnia cena (używając podzapytania)

```sql
SELECT
    id_produktu,
    model,
    cena
FROM produkty
WHERE cena > (SELECT avg(cena)
              FROM produkty)
ORDER BY cena;
```

| id_produktu | model   | cena |
| :---------- | :------ | :--- |
| 2           | EP-200X | 2750 |
| 8           | S-300   | 2999 |
| 17          | L105    | 3150 |
| 3           | EP-300  | 3199 |
| 7           | EZ-550  | 3500 |
| 4           | EE-1000 | 3999 |
| 5           | EE-2000 | 4499 |

<br>

5. ##### Wybiera produkty, które są tańsze niż średnia cena (używając podzapytania)

```sql
SELECT
    id_produktu,
    model,
    cena
FROM produkty
WHERE cena < (SELECT avg(cena)
              FROM produkty)
ORDER BY cena;
```

| id\_produktu | model | cena |
| :--- | :--- | :--- |
| 13 | U3000 | 1550 |
| 10 | X-500 | 1999 |
| 15 | G1000 | 1999 |
| 6 | EX-550 | 2100 |
| 9 | S-400 | 2199 |
| 12 | Z-700 | 2200 |
| 14 | U3001 | 2400 |
| 1 | EP-200 | 2500 |
| 16 | L100 | 2500 |
| 11 | X-600 | 2599 |

<br>

6. ##### Wybiera produkty, które są tańsze niż pierwszy produkt w tabeli

```sql
SELECT
    id_produktu,
    model,
    cena
FROM produkty
WHERE cena < (SELECT cena
              FROM produkty
              WHERE id_produktu = 1)
ORDER BY cena;
```

| id\_produktu | model | cena |
| :--- | :--- | :--- |
| 13 | U3000 | 1550 |
| 10 | X-500 | 1999 |
| 15 | G1000 | 1999 |
| 6 | EX-550 | 2100 |
| 9 | S-400 | 2199 |
| 12 | Z-700 | 2200 |
| 14 | U3001 | 2400 |

<br>

7. ##### `Składnia` prostego `zapytania` z `podzapytaniem`:

```sql
SELECT
    model
FROM (
    SELECT *
    FROM produkty
    ) AS "podzapytanie";
```

| model |
| :--- |
| EE-1000 |
| EE-2000 |
| EP-200 |
| EP-200X |
| EP-300 |
| EX-550 |
| EZ-550 |
| G1000 |
| L100 |
| L105 |
| S-300 |
| S-400 |
| U3000 |
| U3001 |
| X-500 |
| X-600 |
| Z-700 |

> UWAGA!
>
> > - Podzapytanie musi posiadać swój własny **ALIAS**

