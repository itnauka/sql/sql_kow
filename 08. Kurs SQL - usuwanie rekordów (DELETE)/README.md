# 8. Kurs SQL - usuwanie rekordów (DELETE)

<div align="center">
    <a href="https://www.youtube.com/watch?v=IhpqYCDrnBk&list=PL6aekdNhY7DA1wcv-k2MtZxasDeGlre57&index=8">
        <img src="https://i.ytimg.com/vi/IhpqYCDrnBk/hqdefault.jpg" alt="1. Kurs SQL" />
    </a>
</div>

<br>

---

### Przykładowe zapytania SQL

<br>

1. ##### Wyświetla `wszystkie` kolumny w tabeli produkty

```sql
SELECT *
FROM produkty;
```

| id_produktu | id_producenta | model    | kategoria | cena |
| :---------- | :------------ | :------- | :-------- | :--- |
| 1           | 1             | EP-200   | pralka    | 2500 |
| 2           | 1             | EP-200X  | pralka    | 2750 |
| 3           | 1             | EP-300   | pralka    | 3199 |
| 4           | 1             | EE-1000  | telewizor | 3999 |
| 5           | 1             | EE-2000  | telewizor | 4499 |
| 6           | 1             | EX-550   | kuchenka  | 2100 |
| 7           | 1             | EZ-550   | lodówka   | 3500 |
| 8           | 2             | S-300    | pralka    | 2999 |
| 9           | 2             | S-400    | pralka    | 2199 |
| 10          | 2             | X-500    | telewizor | 1999 |
| 11          | 2             | X-600    | telewizor | 2599 |
| 12          | 2             | Z-700    | lodówka   | 2200 |
| 13          | 3             | U3000    | pralka    | 1550 |
| 14          | 3             | U3001    | pralka    | 2400 |
| 15          | 3             | G1000    | kuchenka  | 1999 |
| 16          | 3             | L100     | lodówka   | 2500 |
| 17          | 3             | L105     | lodówka   | 3150 |
| 18          | 1             | EP-350X  | pralka    | 3200 |
| 19          | 2             | S-500    | pralka    | 2149 |
| 20          | 3             | G1500    | kuchenka  | 2349 |
| 21          | 1             | EP-300XS | pralka    | 3399 |

<br>

2. ##### Usuwa rekord z tabeli produkty, gdzie id_produktu wynosi 21

DELETE FROM produkty
WHERE id_produktu = 21;

| id_produktu | id_producenta | model   | kategoria | cena |
| :---------- | :------------ | :------ | :-------- | :--- |
| 1           | 1             | EP-200  | pralka    | 2500 |
| 2           | 1             | EP-200X | pralka    | 2750 |
| 3           | 1             | EP-300  | pralka    | 3199 |
| 4           | 1             | EE-1000 | telewizor | 3999 |
| 5           | 1             | EE-2000 | telewizor | 4499 |
| 6           | 1             | EX-550  | kuchenka  | 2100 |
| 7           | 1             | EZ-550  | lodówka   | 3500 |
| 8           | 2             | S-300   | pralka    | 2999 |
| 9           | 2             | S-400   | pralka    | 2199 |
| 10          | 2             | X-500   | telewizor | 1999 |
| 11          | 2             | X-600   | telewizor | 2599 |
| 12          | 2             | Z-700   | lodówka   | 2200 |
| 13          | 3             | U3000   | pralka    | 1550 |
| 14          | 3             | U3001   | pralka    | 2400 |
| 15          | 3             | G1000   | kuchenka  | 1999 |
| 16          | 3             | L100    | lodówka   | 2500 |
| 17          | 3             | L105    | lodówka   | 3150 |
| 18          | 1             | EP-350X | pralka    | 3200 |
| 19          | 2             | S-500   | pralka    | 2149 |
| 20          | 3             | G1500   | kuchenka  | 2349 |

<br>

4. ##### Usuwa rekordy z tabeli produkty, gdzie id_produktu jest 18, 19 lub 20

DELETE FROM produkty
WHERE id_produktu IN (18, 19, 20);

| id_produktu | id_producenta | model   | kategoria | cena |
| :---------- | :------------ | :------ | :-------- | :--- |
| 1           | 1             | EP-200  | pralka    | 2500 |
| 2           | 1             | EP-200X | pralka    | 2750 |
| 3           | 1             | EP-300  | pralka    | 3199 |
| 4           | 1             | EE-1000 | telewizor | 3999 |
| 5           | 1             | EE-2000 | telewizor | 4499 |
| 6           | 1             | EX-550  | kuchenka  | 2100 |
| 7           | 1             | EZ-550  | lodówka   | 3500 |
| 8           | 2             | S-300   | pralka    | 2999 |
| 9           | 2             | S-400   | pralka    | 2199 |
| 10          | 2             | X-500   | telewizor | 1999 |
| 11          | 2             | X-600   | telewizor | 2599 |
| 12          | 2             | Z-700   | lodówka   | 2200 |
| 13          | 3             | U3000   | pralka    | 1550 |
| 14          | 3             | U3001   | pralka    | 2400 |
| 15          | 3             | G1000   | kuchenka  | 1999 |
| 16          | 3             | L100    | lodówka   | 2500 |
| 17          | 3             | L105    | lodówka   | 3150 |

<br>

5. ##### Usuwa wszystkie rekordy z tabeli produkty

DELETE FROM produkty;

> UWAGA!
>
> > **Zabezpieczenie MySQL:**
> >
> > - You are using safe update mode and you tried to update a table without a WHERE that uses a KEY column
> > - NIE usuniemy bez odblokowania zabzpieczenia

<br>

6. ##### Ustawia tryb SQL_SAFE_UPDATES na 0 (bezpieczne tryby usuwania są wyłączone)

```sql
SET SQL_SAFE_UPDATES = 0;
```

> UWAGA!
>
> > **Zabezpieczenie MySQL:**
> >
> > - Domyślne ustawienie SET SQL_SAFE_UPDATES = 1;
> > - Dodatkowe zabazpieczenie w DataGrip:  
> >   Preferences/Settings | Database | Query Execution | Show warning before running potentially unsafe queries

<br>

```sql
DELETE FROM produkty;
```

| id_produktu | id_producenta | model | kategoria | cena |
| :---------- | :------------ | :---- | :-------- | :--- |

<br>

7. ##### Nadaje uprawnienie DELETE na tabeli produkty użytkownikowi 'root'@'localhost'

```sql
GRANT DELETE ON produkty TO 'root'@'localhost';
```

<br>

8. ##### Usuwa tabelę produkty z bazy danych sklep

```
## -- PRZED -- ##

.
└── sklep
    ├── producenci
    │   ├── id_producenta
    │   ├── nazwa
    │   ├── data_powstania
    │   └── kraj
    └── produkty
        ├── id_produktu
        ├── id_producenta
        ├── model
        ├── kategoria
        └── cena
```

<br>

```sql
DROP TABLE sklep.produkty;
```

```
## -- PO -- ##

.
└── sklep
    └── producenci
        ├── id_producenta
        ├── nazwa
        ├── data_powstania
        └── kraj
```

<br>

8. ##### Usuwa tabelę produkty z bazy danych sklep

`## -- PRZED -- ##`

| Database |
| :------- |
| KursSQL  |
| sklep    |
| ptaki    |

<br>

```sql
DROP TABLE sklep.produkty;
```

`## -- PO -- ##`

| Database |
| :------- |
| KursSQL  |
| ptaki    |