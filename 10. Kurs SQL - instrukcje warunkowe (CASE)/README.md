# 10. Kurs SQL - instrukcje warunkowe (CASE)

<div align="center">
    <a href="https://www.youtube.com/watch?v=Ac6lCi_6-fM&list=PL6aekdNhY7DA1wcv-k2MtZxasDeGlre57&index=10">
        <img src="https://i.ytimg.com/vi/Ac6lCi_6-fM/hqdefault.jpg" alt="1. Kurs SQL" />
    </a>
</div>

<br>

---

### Przykładowe zapytania SQL

<br>

1. ##### Wyświetla `wszystkie` kolumny w tabeli produkty

```sql
SELECT *
FROM produkty;
```

| id_produktu | producent | model   | kategoria | cena |
| :---------- | :-------- | :------ | :-------- | :--- |
| 1           | 1         | EP-200  | pralka    | 2500 |
| 2           | 1         | EP-200X | pralka    | 2750 |
| 3           | 1         | EP-300  | pralka    | 3199 |
| 4           | 1         | EE-1000 | telewizor | 3999 |
| 5           | 1         | EE-2000 | telewizor | 4499 |
| 6           | 1         | EX-550  | kuchenka  | 2100 |
| 7           | 1         | EZ-550  | lodówka   | 3500 |
| 8           | 2         | S-300   | pralka    | 2999 |
| 9           | 2         | S-400   | pralka    | 2199 |
| 10          | 2         | X-500   | telewizor | 1999 |
| 11          | 2         | X-600   | telewizor | 2599 |
| 12          | 2         | Z-700   | lodówka   | 2200 |
| 13          | 3         | U3000   | pralka    | 1550 |
| 14          | 3         | U3001   | pralka    | 2400 |
| 15          | 3         | G1000   | kuchenka  | 1999 |
| 16          | 3         | L100    | lodówka   | 2500 |
| 17          | 3         | L105    | lodówka   | 3150 |

<br>

2. ##### Używa `CASE` do określenia własnej kategorii cenowej

```sql
SELECT
    model,
    cena,
CASE
    WHEN cena <= 2000 THEN 'TANIO'
    WHEN cena >= 3000 THEN 'DROGO'
    ELSE 'ŚREDNIO' # pomiędzy 2000 - 3000
END AS cena_własna
FROM produkty
ORDER BY cena_własna DESC;
```

| model   | cena | cena_własna |
| :------ | :--- | :---------- |
| G1000   | 1999 | TANIO       |
| U3000   | 1550 | TANIO       |
| X-500   | 1999 | TANIO       |
| EP-200X | 2750 | ŚREDNIO     |
| L100    | 2500 | ŚREDNIO     |
| U3001   | 2400 | ŚREDNIO     |
| Z-700   | 2200 | ŚREDNIO     |
| X-600   | 2599 | ŚREDNIO     |
| S-400   | 2199 | ŚREDNIO     |
| S-300   | 2999 | ŚREDNIO     |
| EX-550  | 2100 | ŚREDNIO     |
| EP-200  | 2500 | ŚREDNIO     |
| EZ-550  | 3500 | DROGO       |
| EE-2000 | 4499 | DROGO       |
| EE-1000 | 3999 | DROGO       |
| EP-300  | 3199 | DROGO       |
| L105    | 3150 | DROGO       |

<br>

3. ##### Modyfikuje wynik, aby zawierał podaną cenę, jeśli nie spełnia warunków `CASE`

```sql
SELECT
    model,
    cena,
CASE
    WHEN cena <= 2000 THEN 'TANIO'
    WHEN cena >= 3000 THEN 'DROGO'
    ELSE cena # wyświetli cenę
END AS cena_własna
FROM produkty
ORDER BY cena DESC;
```

| model   | cena | cena_własna |
| :------ | :--- | :---------- |
| G1000   | 1999 | TANIO       |
| U3000   | 1550 | TANIO       |
| X-500   | 1999 | TANIO       |
| L105    | 3150 | DROGO       |
| EP-300  | 3199 | DROGO       |
| EE-1000 | 3999 | DROGO       |
| EE-2000 | 4499 | DROGO       |
| EZ-550  | 3500 | DROGO       |
| S-300   | 2999 | 2999        |
| EP-200X | 2750 | 2750        |
| X-600   | 2599 | 2599        |
| L100    | 2500 | 2500        |
| EP-200  | 2500 | 2500        |
| U3001   | 2400 | 2400        |
| Z-700   | 2200 | 2200        |
| S-400   | 2199 | 2199        |
| EX-550  | 2100 | 2100        |
