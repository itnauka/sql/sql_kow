# 2. Kurs SQL - Sortowanie wyników, klauzula LIKE

<div align="center">
    <a href="https://www.youtube.com/watch?v=gneJA3rXRKg&list=PL6aekdNhY7DA1wcv-k2MtZxasDeGlre57&index=2">
        <img src="https://i.ytimg.com/vi/gneJA3rXRKg/hqdefault.jpg" alt="1. Kurs SQL" />
    </a>
</div>

<br>

---

### Przykładowe zapytania SQL

<br>

1. ##### Wyświetla `wszystkie` kolumny w tabeli produkty

```sql
SELECT *
FROM produkty;
```

| id_produktu | producent | model   | kategoria | cena |
| :---------- | :-------- | :------ | :-------- | :--- |
| 1           | 1         | EP-200  | pralka    | 2500 |
| 2           | 1         | EP-200X | pralka    | 2750 |
| 3           | 1         | EP-300  | pralka    | 3199 |
| 4           | 1         | EE-1000 | telewizor | 3999 |
| 5           | 1         | EE-2000 | telewizor | 4499 |
| 6           | 1         | EX-550  | kuchenka  | 2100 |
| 7           | 1         | EZ-550  | lodówka   | 3500 |
| 8           | 2         | S-300   | pralka    | 2999 |
| 9           | 2         | S-400   | pralka    | 2199 |
| 10          | 2         | X-500   | telewizor | 1999 |
| 11          | 2         | X-600   | telewizor | 2599 |
| 12          | 2         | Z-700   | lodówka   | 2200 |
| 13          | 3         | U3000   | pralka    | 1550 |
| 14          | 3         | U3001   | pralka    | 2400 |
| 15          | 3         | G1000   | kuchenka  | 1999 |
| 16          | 3         | L100    | lodówka   | 2500 |
| 17          | 3         | L105    | lodówka   | 3150 |

<br>

2. ##### Wyświetla `wszystkie` kolumny w tabeli produkty posortowane wg ceny malejąco (DESC)

```sql
SELECT *
FROM produkty
ORDER BY cena DESC;
```

| id_produktu | producent | model   | kategoria | cena |
| :---------- | :-------- | :------ | :-------- | :--- |
| 5           | 1         | EE-2000 | telewizor | 4499 |
| 4           | 1         | EE-1000 | telewizor | 3999 |
| 7           | 1         | EZ-550  | lodówka   | 3500 |
| 3           | 1         | EP-300  | pralka    | 3199 |
| 17          | 3         | L105    | lodówka   | 3150 |
| 8           | 2         | S-300   | pralka    | 2999 |
| 2           | 1         | EP-200X | pralka    | 2750 |
| 11          | 2         | X-600   | telewizor | 2599 |
| 16          | 3         | L100    | lodówka   | 2500 |
| 1           | 1         | EP-200  | pralka    | 2500 |
| 14          | 3         | U3001   | pralka    | 2400 |
| 12          | 2         | Z-700   | lodówka   | 2200 |
| 9           | 2         | S-400   | pralka    | 2199 |
| 6           | 1         | EX-550  | kuchenka  | 2100 |
| 10          | 2         | X-500   | telewizor | 1999 |
| 15          | 3         | G1000   | kuchenka  | 1999 |
| 13          | 3         | U3000   | pralka    | 1550 |

<br>

3. ##### Wyświetla `wszystkie` kolumny w tabeli produkty posortowane wg katergorii malejąco (DESC)

```sql
SELECT *
FROM produkty
ORDER BY kategoria DESC;
```

| id_produktu | producent | model   | kategoria | cena |
| :---------- | :-------- | :------ | :-------- | :--- |
| 4           | 1         | EE-1000 | telewizor | 3999 |
| 5           | 1         | EE-2000 | telewizor | 4499 |
| 11          | 2         | X-600   | telewizor | 2599 |
| 10          | 2         | X-500   | telewizor | 1999 |
| 2           | 1         | EP-200X | pralka    | 2750 |
| 14          | 3         | U3001   | pralka    | 2400 |
| 13          | 3         | U3000   | pralka    | 1550 |
| 9           | 2         | S-400   | pralka    | 2199 |
| 8           | 2         | S-300   | pralka    | 2999 |
| 3           | 1         | EP-300  | pralka    | 3199 |
| 1           | 1         | EP-200  | pralka    | 2500 |
| 7           | 1         | EZ-550  | lodówka   | 3500 |
| 12          | 2         | Z-700   | lodówka   | 2200 |
| 16          | 3         | L100    | lodówka   | 2500 |
| 17          | 3         | L105    | lodówka   | 3150 |
| 6           | 1         | EX-550  | kuchenka  | 2100 |
| 15          | 3         | G1000   | kuchenka  | 1999 |

<br>

4. ##### Wyświetla `wszystkie` kolumny w tabeli produkty posortowane wg katergorii malejąco (DESC)

```sql
SELECT *
FROM produkty
ORDER BY kategoria DESC, cena;
```

| id_produktu | producent | model   | kategoria | cena |
| :---------- | :-------- | :------ | :-------- | :--- |
| 10          | 2         | X-500   | telewizor | 1999 |
| 11          | 2         | X-600   | telewizor | 2599 |
| 4           | 1         | EE-1000 | telewizor | 3999 |
| 5           | 1         | EE-2000 | telewizor | 4499 |
| 13          | 3         | U3000   | pralka    | 1550 |
| 9           | 2         | S-400   | pralka    | 2199 |
| 14          | 3         | U3001   | pralka    | 2400 |
| 1           | 1         | EP-200  | pralka    | 2500 |
| 2           | 1         | EP-200X | pralka    | 2750 |
| 8           | 2         | S-300   | pralka    | 2999 |
| 3           | 1         | EP-300  | pralka    | 3199 |
| 12          | 2         | Z-700   | lodówka   | 2200 |
| 16          | 3         | L100    | lodówka   | 2500 |
| 17          | 3         | L105    | lodówka   | 3150 |
| 7           | 1         | EZ-550  | lodówka   | 3500 |
| 15          | 3         | G1000   | kuchenka  | 1999 |
| 6           | 1         | EX-550  | kuchenka  | 2100 |

<br>

5. ##### Wyświetla `wszystkie` kolumny w tabeli produkty posortowane wg katergorii malejąco oraz ceny malejąco (DESC)

```sql
SELECT *
FROM produkty
ORDER BY kategoria DESC, cena DESC;
```

| id_produktu | producent | model   | kategoria | cena |
| :---------- | :-------- | :------ | :-------- | :--- |
| 5           | 1         | EE-2000 | telewizor | 4499 |
| 4           | 1         | EE-1000 | telewizor | 3999 |
| 11          | 2         | X-600   | telewizor | 2599 |
| 10          | 2         | X-500   | telewizor | 1999 |
| 3           | 1         | EP-300  | pralka    | 3199 |
| 8           | 2         | S-300   | pralka    | 2999 |
| 2           | 1         | EP-200X | pralka    | 2750 |
| 1           | 1         | EP-200  | pralka    | 2500 |
| 14          | 3         | U3001   | pralka    | 2400 |
| 9           | 2         | S-400   | pralka    | 2199 |
| 13          | 3         | U3000   | pralka    | 1550 |
| 7           | 1         | EZ-550  | lodówka   | 3500 |
| 17          | 3         | L105    | lodówka   | 3150 |
| 16          | 3         | L100    | lodówka   | 2500 |
| 12          | 2         | Z-700   | lodówka   | 2200 |
| 6           | 1         | EX-550  | kuchenka  | 2100 |
| 15          | 3         | G1000   | kuchenka  | 1999 |

<br>

6. ##### Wyświetla `wszystkie` kolumny w tabeli produkty gdzie model jest taki jak 'ee-1000' posortowane wg katergorii model

```sql
SELECT *
FROM produkty
WHERE model LIKE 'ee-1000'
ORDER BY model;
```

| id_produktu | producent | model   | kategoria | cena |
| :---------- | :-------- | :------ | :-------- | :--- |
| 4           | 1         | EE-1000 | telewizor | 3999 |

<br>

7. ##### Wyświetla `wszystkie` kolumny w tabeli produkty gdzie model zawiera serię 'ee-' ale jest w wercji 1000 lub wyższej posortowane wg katergorii model

```sql
SELECT *
FROM produkty
WHERE model LIKE 'ee-_000'
ORDER BY model;
```

| id_produktu | producent | model   | kategoria | cena |
| :---------- | :-------- | :------ | :-------- | :--- |
| 4           | 1         | EE-1000 | telewizor | 3999 |
| 5           | 1         | EE-2000 | telewizor | 4499 |

<br>

8. ##### Wyświetla `wszystkie` kolumny w tabeli produkty gdzie model serii zaczyna się od  'e' posortowane wg katergorii model

```sql
SELECT *
FROM produkty
WHERE model LIKE 'e%'
ORDER BY model;
```

| id_produktu | producent | model   | kategoria | cena |
| :---------- | :-------- | :------ | :-------- | :--- |
| 4           | 1         | EE-1000 | telewizor | 3999 |
| 5           | 1         | EE-2000 | telewizor | 4499 |
| 1           | 1         | EP-200  | pralka    | 2500 |
| 2           | 1         | EP-200X | pralka    | 2750 |
| 3           | 1         | EP-300  | pralka    | 3199 |
| 6           | 1         | EX-550  | kuchenka  | 2100 |
| 7           | 1         | EZ-550  | lodówka   | 3500 |

<br>

9. ##### Wyświetla `wszystkie` kolumny w tabeli produkty gdzie modele mają drugą literę "p" i są posortowane wg katergorii model

```sql
SELECT *
FROM produkty
WHERE model LIKE '_p%'
ORDER BY model;
```

| id_produktu | producent | model   | kategoria | cena |
| :---------- | :-------- | :------ | :-------- | :--- |
| 1           | 1         | EP-200  | pralka    | 2500 |
| 2           | 1         | EP-200X | pralka    | 2750 |
| 3           | 1         | EP-300  | pralka    | 3199 |

<br>

10. ##### Wyświetla `wszystkie` kolumny w tabeli produkty gdzie modele mają pierwszą literę "e", drugą literę dowolną następnie posiadają "-" a po niej dowolną liczbę znaków i są posortowane wg katergorii model

```sql
SELECT *
FROM produkty
WHERE model LIKE 'e_-%'
ORDER BY model;
```

| id_produktu | producent | model   | kategoria | cena |
| :---------- | :-------- | :------ | :-------- | :--- |
| 4           | 1         | EE-1000 | telewizor | 3999 |
| 5           | 1         | EE-2000 | telewizor | 4499 |
| 1           | 1         | EP-200  | pralka    | 2500 |
| 2           | 1         | EP-200X | pralka    | 2750 |
| 3           | 1         | EP-300  | pralka    | 3199 |
| 6           | 1         | EX-550  | kuchenka  | 2100 |
| 7           | 1         | EZ-550  | lodówka   | 3500 |

<br>

10. ##### Wyświetla `wszystkie` kolumny w tabeli produkty gdzie modele mają pierwszą literę "e", drugą literę dowolną następnie posiadają znak "-", kolejna to dowolny jeden znak i kończą się dwoma zerami (00). 

```sql
SELECT *
FROM produkty
WHERE model LIKE 'e_-_00'
ORDER BY model;
```

| id_produktu | producent | model  | kategoria | cena |
| :---------- | :-------- | :----- | :-------- | :--- |
| 1           | 1         | EP-200 | pralka    | 2500 |
| 3           | 1         | EP-300 | pralka    | 3199 |
